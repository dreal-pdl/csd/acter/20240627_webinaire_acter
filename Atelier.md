# Atelier 1 : Quels sont les 2 activités prioritaires à vos yeux ?

Maintenir et développer les publications reproductibles (RPLS, EPTB)  
Maintenir et développer {datalibaba}  
Maintenir et développer {gouvdown}  
Maintenir et développer {COGiter}  
Maintenir et développer {mapfactory}  
Maintenir et développer {shinygouv}  

Squelette d'un tableau de bord à la marque Etat  
Nouvelles publications /  datavisualisation  
Script de collecte / traitement de sources de données (géo ou non)  
Bancarisation indicateurs / Administration de données (géo ou non)  




# Atelier 2 Quelles sont les deux formations prioritaires à vos yeux ?
Rmarkdown et solutions de déploiement  
RShiny et solutions de déploiement  
Golem  
Packages  
Bigdata  
R pour l'ADL  
Git/gitlab et l'intégration continue   

Organisation d'atelier (Quarto, parquet, Bonnes pratiques, ...)  
Animation communauté (connaissance des travaux existants...)  


# Atelier 3 : Quel type d'accompagnement à la demande ?

Cadrage  
Support technique  
Compagnonnage  
Audit / optimisation  
Accompagnement de prestations comprenant du développement en R  
Solutions de déploiement des produits  

# Atelier 4 : quel axe faut-il privilégier à vos yeux ?  

Produits reproductibles et briques (packages R)  
Montée en compétences  
Conseil et expertise  
